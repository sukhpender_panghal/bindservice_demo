package com.example.bindservice_demo

import android.app.ActivityManager
import android.content.*
import android.os.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var boundService: BindService? = null
    var isBound = false
    val handlr: Handler = object : Handler(Looper.myLooper()!!) {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            val b = msg.data
            val latti = b.getString("KEY1")
            val longii = b.getString("KEY2")
            txt_latitude.setText(latti)
            txt_longitude.setText(longii)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val serviceClass = BindService::class.java
        val intent = Intent(applicationContext, serviceClass)

        /**code to receive data from handler coming from service class*/
        intent.putExtra("handler", Messenger(handlr))

        btn_start.setOnClickListener {
            if (isServiceRunning(serviceClass) == false) {
                startService(intent)
                bindService(intent, boundServiceConnection, BIND_AUTO_CREATE);
            } else {
                Toast.makeText(this, "alrdy running", Toast.LENGTH_SHORT).show()
            }
        }
        btn_bind.setOnClickListener {
            boundService?.locationFetch()
        }
    }

    private fun isServiceRunning(serviceClass: Class<BindService>): Any {
        val activityManager =
                getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    override fun onStop() {
        super.onStop()
        if (isBound) {
            unbindService(boundServiceConnection)
            isBound = false
        }
    }

    var boundServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(p0: ComponentName?) {
            isBound = false
            boundService = null
        }

        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binderBridge = p1 as BindService.MyBinder
            boundService = binderBridge.service
            isBound = true
        }
    }
}