package com.example.bindservice_demo

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.os.*
import android.util.Log
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices

class BindService: Service() {
    private var mHandler: Messenger? = null
    private val localBinder: IBinder = MyBinder()

    override fun onBind(p0: Intent?): IBinder? {
        return localBinder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        mHandler = intent?.getParcelableExtra<Messenger>("handler")
        return super.onStartCommand(intent, flags, startId)

    }

    /**function for binding service to activity*/
    @SuppressLint("MissingPermission")
    fun locationFetch() {
        val fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(this)
        val locationRequest =
            LocationRequest().setInterval(5000).setFastestInterval(5000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        fusedLocationClient.requestLocationUpdates(
            locationRequest, object : LocationCallback() {
                override fun onLocationResult(p0: LocationResult?) {
                    super.onLocationResult(p0)

                    /**code for getting latlong from locationResult*/
                    for (location in p0!!.locations) {
                        Log.e("latitude", "$location.latitude.toString()")
                        Log.e("longitude", "$location.longitude.toString()")

                        val lati = location.latitude.toString()
                        val longi = location.longitude.toString()

                        /**code for thread to pass data to the activity*/
                        val msg = Message()
                        val b = Bundle()
                        val value1 = lati
                        val value2 = longi
                        b.putString("KEY1", value1)
                        b.putString("KEY2", value2)
                        msg.data = b
                        mHandler?.send(msg)
                    }
                }
            }, Looper.myLooper()
        )
    }

    inner class MyBinder : Binder() {
        val service: BindService
            get() = this@BindService
    }
}